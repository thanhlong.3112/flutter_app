import 'package:faker/faker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/model/order_item.dart';
import 'package:flutter_app/model/product_item.dart';

import 'dart:convert';
import 'package:http/http.dart' as http;

class OrderProvider extends ChangeNotifier {
  final List<OrderItem> _items = [];

  List<OrderItem> get items {
    return [..._items];
  }

  // void addOrder(List<ProductItem> productItems) {
  //   _items.add(OrderItem(
  //     id: DateTime.now().toString(),
  //     name: faker.sport.name(),
  //     price: productItems.fold(0, (previousValue, element) {
  //       return previousValue + element.price * element.quantity;
  //     }),
  //     createdAt: DateTime.now(),
  //     updatedAt: DateTime.now(),
  //     products: [...productItems],
  //   ));
  //   notifyListeners();
  // }

  Future<List<OrderItem>> fetchOrders() async {
    try {
      final response = await http.get(Uri.parse(
          'https://gun-gangster-arena-91012185.firebaseio.com/orders.json'));
      final parsed = json.decode(response.body) as Map<String, dynamic>?;
      final List<OrderItem> loadedOrders = [];
      parsed?.forEach((key, value) {
        loadedOrders.add(OrderItem.fromJson(value).copyWith(id: key));
      });
      return loadedOrders;
    } catch (error) {
      rethrow;
    }
  }

  Future addOrder(List<ProductItem> productItems) async {
    try {
      final order = OrderItem(
        id: DateTime.now().toString(),
        name: faker.sport.name(),
        price: productItems.fold(0, (previousValue, element) {
          return previousValue + element.price * element.quantity;
        }),
        createdAt: DateTime.now(),
        updatedAt: DateTime.now(),
        products: [...productItems],
      );
      final url = Uri.parse(
          'https://gun-gangster-arena-91012185.firebaseio.com/orders.json');
      final response = await http.post(url, body: jsonEncode(order.toJson()));
      if (response.statusCode == 200) {
        // success requesst
        final extractedData =
            json.decode(response.body) as Map<String, dynamic>?;
        if (extractedData == null) {
          return;
        }
        _items.add(order.copyWith(id: extractedData['name']));
        notifyListeners();
      }
    } catch (e) {
      rethrow;
    }
  }
}
