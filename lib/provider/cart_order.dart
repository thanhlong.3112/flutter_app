import 'package:flutter/material.dart';
import 'package:flutter_app/model/product_item.dart';

class CartOrderProvider extends ChangeNotifier {
  final List<ProductItem> _selectedItem = [];
  List<ProductItem> get selectedItem => _selectedItem;

  final List<ProductItem> _items = [];
  List<ProductItem> get cartItems => _items;

  bool deleteForm = false;
  void changeForm() {
    deleteForm = !deleteForm;
    notifyListeners();
  }

  void clearCart() {
    deleteForm = false;
    _items.clear();
    _selectedItem.clear();
    notifyListeners();
  }

  double get totalPrice {
    double total = 0;

    for (var element in _items) {
      total += element.price * element.quantity;
    }
    return total;
  }

  void deleteItem(List<bool> selectedItem) {
    for (int i = 0; i < _items.length; i++) {
      if (selectedItem[i] == true) {
        _items.removeAt(i);
      }
    }
    notifyListeners();
  }

  void addToCart(ProductItem orderProduct) {
    final productIndex = _items.indexWhere((e) => e.id == orderProduct.id);
    print(orderProduct.title);
    if (productIndex < 0) {
      _items.add(orderProduct);
    } else {
      _items[productIndex] =
          orderProduct.copyWith(quantity: _items[productIndex].quantity + 1);
    }
    notifyListeners();
  }

  void degreeProd(ProductItem orderProduct) {
    final productIndex = _items.indexWhere((e) => e.id == orderProduct.id);
    if (_items[productIndex].quantity > 1) {
      _items[productIndex] =
          orderProduct.copyWith(quantity: _items[productIndex].quantity - 1);
    } else {
      _items.removeAt(productIndex);
    }
    notifyListeners();
  }

  void onTongleSelected(ProductItem orderProduct) {
    final productIndex =
        _selectedItem.indexWhere((e) => e.id == orderProduct.id);
    if (productIndex < 0) {
      _selectedItem.add(orderProduct);
    } else {
      _selectedItem.removeWhere((e) => e.id == orderProduct.id);
    }
    notifyListeners();
  }

  bool isSelected(ProductItem orderProduct) {
    return _selectedItem.indexWhere((e) => e.id == orderProduct.id) >= 0;
  }

  void deleteSelected() {
    for (var element in _selectedItem) {
      _items.removeWhere((e) => e.id == element.id);
    }
    _selectedItem.clear();
    notifyListeners();
  }

  void undoDelete(List<ProductItem> _oldSelectedItem) {
    _items.addAll(_oldSelectedItem);
    _selectedItem.addAll(_oldSelectedItem);
    notifyListeners();
  }
}
