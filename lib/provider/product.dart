import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_app/model/product_item.dart';
import 'package:http/http.dart' as http;

class ProductProvider extends ChangeNotifier {
  final List<ProductItem> _items = []; // init default product items

  List<ProductItem> get items => _items;

  Future<void> fetchProducts() async {
    try {
      final url = Uri.parse(
          'https://flutter1-d9f6d-default-rtdb.firebaseio.com/products.json');
      final response = await http.get(url);
      if (response.statusCode == 200) {
        // success requesst
        final extractedData =
            json.decode(response.body) as Map<String, dynamic>?;
        if (extractedData == null) {
          return;
        }
        _items.clear();
        extractedData.forEach((key, value) =>
            _items.add(ProductItem.fromJson(value).copyWith(id: key)));
        notifyListeners();
      }
    } catch (e) {
      rethrow;
    }
  }

  Future<void> addProduct(ProductItem productItem) async {
    try {
      final url = Uri.parse(
          'https://flutter1-d9f6d-default-rtdb.firebaseio.com/products.json');
      final response =
          await http.post(url, body: jsonEncode(productItem.toJson()));
      if (response.statusCode == 200) {
        // success requesst
        final extractedData =
            json.decode(response.body) as Map<String, dynamic>?;
        if (extractedData == null) {
          return;
        }
        _items.add(productItem.copyWith(id: extractedData['name']));
        notifyListeners();
      }
    } catch (e) {
      rethrow;
    }
  }

  Future<void> removeProduct(ProductItem productItem) async {
    try {
      final url = Uri.parse(
          'https://flutter1-d9f6d-default-rtdb.firebaseio.com/${productItem.id}.json');
      final response = await http.delete(url);
      if (response.statusCode == 200) {
        // success requesst
        final extractedData =
            json.decode(response.body) as Map<String, dynamic>?;
        if (extractedData == null) {
          return;
        }
        _items.removeWhere((element) => productItem.id == element.id);
        notifyListeners();
      }
    } catch (e) {
      rethrow;
    }
  }

  Future<void> updateProduct(ProductItem productItem) async {
    try {
      final url = Uri.parse(
          'https://flutter1-d9f6d-default-rtdb.firebaseio.com/${productItem.id}.json');
      final response =
          await http.put(url, body: jsonEncode(productItem.toJson()));
      if (response.statusCode == 200) {
        // success requesst
        final extractedData =
            json.decode(response.body) as Map<String, dynamic>?;
        if (extractedData == null) {
          return;
        }
        final index =
            _items.indexWhere((element) => productItem.id == element.id);
        _items[index] = productItem;
        notifyListeners();
      }
    } catch (e) {
      rethrow;
    }
  }
}




  // void favoriteMode(ProductItem productItem) {
  //   final index = _items.indexWhere((element) => productItem.id == element.id);
  //   _items[index].isFavorite = !_items[index].isFavorite;
  //   notifyListeners();
  // }




// final List<ProductItem> productItems = <ProductItem>[
//   ProductItem(
//       id: '1',
//       name: 'Product 1',
//       price: 100,
//       quantity: 1,
//       image:
//           'https://flutter.github.io/assets-for-api-docs/assets/widgets/owl.jpg',
//       description: 'Ok',
//       isFavorite: false),
//   ProductItem(
//       id: '2',
//       name: 'Product 2',
//       price: 100,
//       quantity: 1,
//       image:
//           'https://flutter.github.io/assets-for-api-docs/assets/widgets/owl.jpg',
//       description: 'Ok',
//       isFavorite: false),
//   ProductItem(
//       id: '3',
//       name: 'Product 2',
//       price: 100,
//       quantity: 1,
//       image:
//           'https://flutter.github.io/assets-for-api-docs/assets/widgets/owl.jpg',
//       description: 'Ok',
//       isFavorite: false),
//   ProductItem(
//       id: '4',
//       name: 'Product 2',
//       price: 100,
//       quantity: 1,
//       image:
//           'https://flutter.github.io/assets-for-api-docs/assets/widgets/owl.jpg',
//       description: 'Ok',
//       isFavorite: false),
// ];

