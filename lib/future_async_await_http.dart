import 'dart:convert' as convert;

import 'package:http/http.dart' as http;

void main(List<String> arguments) async {
  // This example uses the Google Books API to search for books about http.
  // https://developers.google.com/books/docs/overview
  var newUrl = Uri.https('api.themoviedb.org', '/3/movie/popular', {
    'api_key': '{65302d9fd57dda3ea2ba86f370ab6b7f}',
    'language': '{en-US}',
    'page': '{1}'
  });

  // Await the http get response, then decode the json-formatted response.
  var newResponse = await http.get(newUrl);
  if (newResponse.statusCode == 200) {
    var jsonResponse = convert.jsonDecode(newResponse.body);

    print('Number of books about http: $jsonResponse.');
  } else {
    print('Request failed with status: ${newResponse.statusCode}.');
  }
}
