class ProductItem {
  final String id;
  final String title;
  final String imageUrl;
  final double price;
  final int quantity;
  final String description;

  ProductItem({
    required this.id,
    required this.title,
    required this.imageUrl,
    required this.price,
    required this.quantity,
    required this.description,
  });

  copyWith({
    String? id,
    String? title,
    String? imageUrl,
    double? price,
    int? quantity,
    String? description,
  }) {
    return ProductItem(
      id: id ?? this.id,
      title: title ?? this.title,
      imageUrl: imageUrl ?? this.imageUrl,
      price: price ?? this.price,
      quantity: quantity ?? this.quantity,
      description: description ?? this.description,
    );
  }

  factory ProductItem.fromJson(Map<String, dynamic> json) {
    return ProductItem(
      id: json['id'] as String,
      title: json['title'] as String,
      imageUrl: json['imageUrl'] as String,
      price: json['price'] as double,
      quantity: json['quantity'] as int,
      description: json['description'] as String,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'title': title,
      'imageUrl': imageUrl,
      'price': price,
      'quantity': quantity,
      'description': description,
    };
  }
}
