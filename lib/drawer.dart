import 'package:flutter/material.dart';
import 'package:flutter_app/screen/product_list_screen.dart';
import 'package:flutter_app/screen/order_screen.dart';
import 'package:flutter_app/screen/shop_product_list_screen.dart';

class DrawerMenu extends StatelessWidget {
  const DrawerMenu({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        // Important: Remove any padding from the ListView.
        padding: EdgeInsets.zero,
        children: [
          const DrawerHeader(
            decoration: BoxDecoration(
              color: Colors.blue,
            ),
            child: Text('Drawer Header'),
          ),
          ListTile(
            title: const Text('Your Product Screen'),
            onTap: () {
              Navigator.pushReplacementNamed(
                  context, ShopProductListScreen.routeName);
            },
          ),
          ListTile(
            title: const Text('My Shop Screen '),
            onTap: () {
              Navigator.pushReplacementNamed(
                  context, ProductListScreen.routeName);
            },
          ),
          ListTile(
            title: const Text('Order Screen '),
            onTap: () {
              Navigator.pushReplacementNamed(context, OrderScreen.routeName);
            },
          ),
        ],
      ),
    );
  }
}
