import 'package:flutter/material.dart';
import 'package:flutter_app/model/order_item.dart';
import 'package:flutter_app/provider/order.dart';

import 'package:provider/provider.dart';

import '../drawer.dart';

class OrderScreen extends StatelessWidget {
  static const routeName = '/order-screen';
  const OrderScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const DrawerMenu(),
      appBar: AppBar(
        title: const Text('Order Screen'),
      ),
      body: Builder(builder: (context) {
        return FutureBuilder<List<OrderItem>>(
            future: Provider.of<OrderProvider>(context, listen: false)
                .fetchOrders(),
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return const Center(
                  child: CircularProgressIndicator(),
                );
              }
              if (snapshot.hasError) {
                return Center(
                  child: Text('Error: ${snapshot.error}'),
                );
              }
              final orderItems = snapshot.data ?? [];
              return ListView.separated(
                  itemBuilder: (context, index) {
                    final orderItem = orderItems[index];
                    return Column(
                      children: [
                        ListTile(
                          title: Text(orderItem.name,
                              style: Theme.of(context).textTheme.headline6),
                          subtitle: Text(orderItem.price.toString()),
                        ),
                        ListView.separated(
                            physics: const ClampingScrollPhysics(),
                            shrinkWrap: true,
                            itemBuilder: (context, productIndex) {
                              final product = orderItem.products[productIndex];
                              return ListTile(
                                title: Text(product.title),
                                subtitle: Text(
                                    '${product.price} x ${product.quantity}'),
                              );
                            },
                            separatorBuilder: (context, index) =>
                                const Divider(),
                            itemCount: orderItem.products.length),
                      ],
                    );
                  },
                  separatorBuilder: (context, index) => const Divider(),
                  itemCount: orderItems.length);
            });
      }),
    );
  }
}
