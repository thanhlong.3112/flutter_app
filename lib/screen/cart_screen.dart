import 'package:flutter/material.dart';
import 'package:flutter_app/provider/cart_order.dart';
import 'package:flutter_app/provider/order.dart';
import 'package:provider/provider.dart';

import 'order_screen.dart';

class CartOrderScreen extends StatelessWidget {
  static const routeName = '/da';
  const CartOrderScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Your Cart'),
        ),
        body: Builder(
          builder: (context) {
            final cartOrder =
                Provider.of<CartOrderProvider>(context, listen: true);
            final cartItems = cartOrder.cartItems;
            final totalPrice = cartOrder.totalPrice;
            return ListView(
              padding: const EdgeInsets.all(10),
              children: [
                Card(
                  child: Padding(
                    padding: const EdgeInsets.all(8),
                    child: Row(
                      children: [
                        const Expanded(
                          flex: 1,
                          child: Text(
                            'Total',
                            style: TextStyle(fontSize: 20),
                          ),
                        ),
                        Expanded(
                          flex: 6,
                          child: SingleChildScrollView(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                Container(
                                  padding: const EdgeInsets.symmetric(
                                    horizontal: 10,
                                    vertical: 5,
                                  ),
                                  decoration: BoxDecoration(
                                    color: Colors.blue,
                                    borderRadius: BorderRadius.circular(50),
                                  ),
                                  child: Text(
                                    '\$$totalPrice',
                                    style: const TextStyle(
                                      fontSize: 16,
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                                const SizedBox(width: 5),
                                ElevatedButton(
                                  child: const Text(
                                    'ORDER NOW',
                                    style: TextStyle(
                                      fontSize: 16,
                                      color: Colors.white,
                                    ),
                                  ),
                                  onPressed: () {
                                    Provider.of<OrderProvider>(context,
                                            listen: false)
                                        .addOrder(cartItems);
                                    cartOrder.clearCart();
                                    Navigator.of(context)
                                        .popAndPushNamed(OrderScreen.routeName);
                                  },
                                ),
                                const SizedBox(width: 5),
                                ElevatedButton(
                                  style: ElevatedButton.styleFrom(
                                    primary: Colors.white,
                                    shape: RoundedRectangleBorder(
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(4)),
                                      side: BorderSide(
                                        color: Colors.red,
                                        width: 1.0,
                                      ),
                                    ),
                                  ),
                                  child: Text(
                                    '${cartOrder.deleteForm ? "Delete Selected" : 'Delete'}',
                                    style: TextStyle(
                                      fontSize: 16,
                                      color: Colors.red,
                                    ),
                                  ),
                                  onPressed: () {
                                    cartOrder.changeForm();
                                  },
                                ),
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                Container(
                  padding: const EdgeInsets.only(top: 20),
                  height: MediaQuery.of(context).size.height - 200,
                  child: ListView.builder(
                      itemCount: cartItems.length,
                      itemBuilder: (context, index) {
                        return Card(
                          child: Row(
                            children: [
                              if (cartOrder.deleteForm)
                                Checkbox(
                                    value:
                                        cartOrder.isSelected(cartItems[index]),
                                    onChanged: (bool? value) {
                                      cartOrder
                                          .onTongleSelected(cartItems[index]);
                                    }),
                              Container(
                                margin: const EdgeInsets.all(5),
                                alignment: Alignment.center,
                                width: 60,
                                height: 60,
                                decoration: BoxDecoration(
                                  color: Colors.blue,
                                  borderRadius: BorderRadius.circular(50),
                                ),
                                child: Text(
                                  '\$${cartItems[index].price.toString()}',
                                  style: const TextStyle(color: Colors.white),
                                ),
                              ),
                              Expanded(
                                flex: 3,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      cartItems[index].title,
                                      style: const TextStyle(
                                        fontSize: 16,
                                      ),
                                    ),
                                    Text(
                                      'Total: \$' +
                                          (cartItems[index].price *
                                                  cartItems[index].quantity)
                                              .toString(),
                                      style: const TextStyle(
                                        color: Colors.grey,
                                        fontSize: 14,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Expanded(
                                flex: 3,
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    IconButton(
                                        onPressed: () {
                                          cartOrder.degreeProd(
                                            cartItems[index],
                                          );
                                        },
                                        icon: const Icon(
                                          Icons.remove,
                                          color: Colors.red,
                                        )),
                                    const SizedBox(width: 5),
                                    Text(
                                      'x' +
                                          cartItems[index].quantity.toString(),
                                      textAlign: TextAlign.center,
                                      style: const TextStyle(
                                        fontSize: 16,
                                      ),
                                    ),
                                    const SizedBox(width: 5),
                                    IconButton(
                                        onPressed: () {
                                          cartOrder.addToCart(
                                            cartItems[index],
                                          );
                                        },
                                        icon: const Icon(Icons.add,
                                            color: Colors.blue)),
                                  ],
                                ),
                              )
                            ],
                          ),
                        );
                      }),
                ),
                if (cartOrder.deleteForm)
                  cartItems.length > 0
                      ? Padding(
                          padding: const EdgeInsets.symmetric(
                            horizontal: 25,
                            vertical: 10,
                          ),
                          child: SizedBox(
                            width: double.infinity,
                            child: ElevatedButton(
                              child: Text(
                                "Delete ()",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 18,
                                ),
                              ),
                              onPressed: () {
                                final selectedItem = [
                                  ...cartOrder.selectedItem
                                ];
                                cartOrder.deleteSelected();
                                ScaffoldMessenger.of(context).showSnackBar(
                                  SnackBar(
                                    content:
                                        const Text('Deleted selected items'),
                                    action: SnackBarAction(
                                      label: 'Undo',
                                      onPressed: () {
                                        cartOrder.undoDelete(selectedItem);
                                        cartOrder.changeForm();
                                      },
                                    ),
                                  ),
                                );
                              },
                            ),
                          ),
                        )
                      : Container(),
              ],
            );
          },
        ));
  }
}
