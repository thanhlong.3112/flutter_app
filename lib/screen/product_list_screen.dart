import 'package:flutter/material.dart';
import 'package:flutter_app/provider/cart_order.dart';
import 'package:flutter_app/provider/product.dart';
import 'package:flutter_app/screen/product_detail_screen.dart';
import 'package:provider/provider.dart';
import 'package:alan_voice/alan_voice.dart';
import '../drawer.dart';
import 'cart_screen.dart';

class ProductListScreen extends StatefulWidget {
  static const routeName = '/product-list';
  const ProductListScreen({Key? key}) : super(key: key);

  @override
  _ProductListScreenState createState() => _ProductListScreenState();
}

class _ProductListScreenState extends State<ProductListScreen> {
  _MyHomePageState() {
    /// Init Alan Button with project key from Alan Studio
    AlanVoice.addButton(
        "dad035de1dce25d5611e6130bbed73622e956eca572e1d8b807a3e2338fdd0dc/stage",
        buttonAlign: AlanVoice.BUTTON_ALIGN_LEFT);

    /// Handle commands from Alan Studio
    AlanVoice.onCommand.add((command) {
      debugPrint("got new command ${command.toString()}");
    });
  }

  Future<void> _refreshProducts(BuildContext context) async {
    await Provider.of<ProductProvider>(context, listen: false).fetchProducts();
  }

  @override
  void initState() {
    super.initState();
    _refreshProducts(context);
  }

  @override
  Widget build(BuildContext context) {
    var screenWidth = MediaQuery.of(context).size.width;

    final productContainer = Provider.of<ProductProvider>(context);

    return Scaffold(
        drawer: const DrawerMenu(),
        appBar: AppBar(
          title: const Text('Product List'),
          actions: [
            Stack(
              children: [
                IconButton(
                    onPressed: () {
                      Navigator.pushNamed(context, CartOrderScreen.routeName);
                    },
                    icon: const Icon(Icons.shopping_cart)),
                Positioned(
                  right: 5,
                  top: 10,
                  child: Container(
                    padding: const EdgeInsets.all(1),
                    decoration: BoxDecoration(
                      color: Colors.red,
                      borderRadius: BorderRadius.circular(8.5),
                    ),
                    constraints: const BoxConstraints(
                      minWidth: 15,
                      minHeight: 15,
                    ),
                    child: Text(
                      '${Provider.of<CartOrderProvider>(context, listen: true).cartItems.length}',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 10,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                )
              ],
            )
          ],
        ),
        body: RefreshIndicator(
          onRefresh: () => _refreshProducts(context),
          child: GridView.builder(
            padding: const EdgeInsets.all(15),
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: screenWidth > 600 ? 3 : 2,
              mainAxisSpacing: 20,
              crossAxisSpacing: 20,
              childAspectRatio: 3 / 2,
            ),
            itemCount: productContainer.items.length,
            itemBuilder: (context, index) {
              final product = productContainer.items[index];
              return ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: GridTile(
                  child: GestureDetector(
                    onTap: () {
                      Navigator.pushNamed(
                          context, ProductDetailScreen.routeName,
                          arguments: product);
                    },
                    child: Image(
                      image: NetworkImage(product.imageUrl),
                      fit: BoxFit.cover,
                    ),
                  ),
                  footer: GridTileBar(
                      backgroundColor: Colors.black87,
                      leading: IconButton(
                        onPressed: () {},
                        icon: Icon(Icons.favorite),
                      ),
                      // leading: IconButton(
                      //   onPressed: () {
                      //     productContainer.favoriteMode(product);
                      //   },
                      //   icon: product.isFavorite
                      //       ? Icon(
                      //           Icons.favorite,
                      //           color: Colors.red,
                      //         )
                      //       : Icon(
                      //           Icons.favorite,
                      //           color: Colors.white,
                      //         ),
                      // ),
                      title: Text(
                        product.title,
                        textAlign: TextAlign.center,
                      ),
                      trailing: IconButton(
                        icon: Icon(Icons.shopping_cart),
                        onPressed: () {
                          Provider.of<CartOrderProvider>(context, listen: false)
                              .addToCart(product);
                        },
                      )),
                ),
              );
            },
          ),
        ));
  }
}
