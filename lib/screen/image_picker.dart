import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';

class MyPage extends StatefulWidget {
  @override
  _MyPageState createState() => _MyPageState();
}

class _MyPageState extends State<MyPage> {
  File? image;

  Future pickImage(ImageSource source) async {
    try {
      final image = await ImagePicker().pickImage(source: source);
      if (image == null) return;
      final imageTemporary = File(image.path);
      setState(() => this.image = imageTemporary);
    } on PlatformException catch (e) {
      print('Failed to pick image: $e');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Image Picker"),
        ),
        body: Center(
          child: Stack(children: <Widget>[
            image == null
                ? ClipOval(
                    child: Container(
                      height: 150,
                      width: 150,
                      color: Colors.amber,
                    ),
                  )
                : ClipOval(
                    child: Container(
                      height: 150,
                      width: 150,
                      child: Image.file(
                        image!,
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
            Positioned(
              bottom: 20.0,
              right: 20.0,
              child: InkWell(
                onTap: () {
                  showModalBottomSheet(
                    context: context,
                    builder: ((builder) => bottomSheet()),
                  );
                },
                child: Icon(
                  Icons.camera_alt,
                  color: Colors.teal,
                  size: 28.0,
                ),
              ),
            ),
          ]),
        ));
  }

  Widget bottomSheet() {
    return Column(mainAxisSize: MainAxisSize.min, children: [
      ListTile(
        leading: Icon(Icons.camera_alt),
        title: Text('Camera'),
        onTap: () => pickImage(ImageSource.camera),
      ), // ListTile
      ListTile(
          leading: Icon(Icons.image),
          title: Text('Gallery'),
          onTap: () => pickImage(
                ImageSource.gallery,
              ))
    ]);
  }
}
