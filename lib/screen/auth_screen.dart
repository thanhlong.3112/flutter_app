import 'package:flutter/material.dart';
import 'package:flutter_app/provider/auth.dart';
import 'package:provider/provider.dart';

enum AuthMode { signup, login }

class AuthScreen extends StatefulWidget {
  static const routeName = '/auth';
  const AuthScreen({Key? key, this.counter}) : super(key: key);
  final int? counter;

  @override
  State<AuthScreen> createState() => _AuthScreenState();
}

class _AuthScreenState extends State<AuthScreen> {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  @override
  void initState() {
    debugPrint('Auth-initState');
    super.initState();
  }

  @override
  void didChangeDependencies() {
    debugPrint('Auth-didChangeDependencies');
    super.didChangeDependencies();
  }

  @override
  void didUpdateWidget(covariant AuthScreen oldWidget) {
    debugPrint('Auth-didUpdateWidget');
    super.didUpdateWidget(oldWidget);
  }

  @override
  void reassemble() {
    debugPrint('Auth-reassemble');
    super.reassemble();
  }

  @override
  void deactivate() {
    debugPrint('Auth-deactivate');
    super.deactivate();
  }

  @override
  void dispose() {
    debugPrint('Auth-dispose');
    super.dispose();
  }

  @override
  void setState(VoidCallback fn) {
    debugPrint('Auth-setState');
    super.setState(fn);
  }

  final ValueNotifier<bool> _authMode = ValueNotifier<bool>(true);
  void _switchAuthMode() {
    if (_authMode.value == false) {
      _authMode.value = true;
    } else {
      _authMode.value = false;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Auth Page'),
      ),
      body: Stack(
        children: [
          Container(
            decoration: const BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [Color(0xffb7a1f0), Color(0xfffadbbb)],
              ),
            ),
          ),
          Center(
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Container(
                    margin: const EdgeInsets.symmetric(
                        horizontal: 20, vertical: 10),
                    padding: const EdgeInsets.symmetric(
                        horizontal: 50, vertical: 10),
                    transform: Matrix4.rotationZ(-0.1),
                    decoration: BoxDecoration(
                      color: Colors.deepOrange.shade900,
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Text(
                      'My Shop ',
                      textAlign: TextAlign.center,
                      style: Theme.of(context)
                          .textTheme
                          .headline1
                          ?.copyWith(color: Colors.white),
                    ),
                  ),
                  Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15.0),
                    ),
                    elevation: 8.0,
                    margin: const EdgeInsets.all(20),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Form(
                        child: Column(
                          children: [
                            TextFormField(
                              controller: _emailController,
                              decoration: const InputDecoration(
                                labelText: 'Email',
                              ),
                              style: Theme.of(context).textTheme.headline6,
                            ),
                            TextFormField(
                              controller: _passwordController,
                              decoration: const InputDecoration(
                                labelText: 'Password',
                              ),
                              obscureText: true,
                              style: Theme.of(context).textTheme.headline6,
                            ),
                            ValueListenableBuilder<bool>(
                              valueListenable: _authMode,
                              builder: (BuildContext context, bool value,
                                  Widget? child) {
                                return value
                                    ? SizedBox(
                                        height: 0,
                                      )
                                    : TextFormField(
                                        decoration: const InputDecoration(
                                          labelText: 'Confirm Password',
                                        ),
                                        obscureText: true,
                                      );
                              },
                            ),
                            const SizedBox(
                              height: 20,
                            ),
                            Provider.of<AuthProvider>(context, listen: true)
                                    .isLoading
                                ? const CircularProgressIndicator()
                                : ElevatedButton(
                                    onPressed: () {
                                      // Navigator.pop(context, _authMode);
                                      if (_authMode.value) {
                                        Provider.of<AuthProvider>(context,
                                                listen: false)
                                            .login(
                                          _emailController.text,
                                          _passwordController.text,
                                        );
                                      } else {
                                        Provider.of<AuthProvider>(context,
                                                listen: false)
                                            .signup(
                                          _emailController.text,
                                          _passwordController.text,
                                        );
                                      }
                                      ValueListenableBuilder<bool>(
                                        valueListenable: _authMode,
                                        builder: (BuildContext context,
                                            bool value, Widget? child) {
                                          return value
                                              ? Text('LOGIN')
                                              : Text('SIGN UP');
                                        },
                                      );
                                    },
                                    child: ValueListenableBuilder<bool>(
                                      valueListenable: _authMode,
                                      builder: (BuildContext context,
                                          bool value, Widget? child) {
                                        return value
                                            ? Text('LOGIN')
                                            : Text('SIGN UP');
                                      },
                                    ),
                                  ),
                            const SizedBox(
                              height: 10,
                            ),
                            Provider.of<AuthProvider>(context, listen: true)
                                    .isLoading
                                ? const CircularProgressIndicator()
                                : TextButton(
                                    onPressed: _switchAuthMode,
                                    child: ValueListenableBuilder<bool>(
                                      valueListenable: _authMode,
                                      builder: (BuildContext context,
                                          bool value, Widget? child) {
                                        return value
                                            ? Text('SIGN UP')
                                            : Text('LOGIN');
                                      },
                                    ),
                                    style: TextButton.styleFrom(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 10.0, vertical: 4),
                                      textStyle:
                                          Theme.of(context).textTheme.headline6,
                                      tapTargetSize:
                                          MaterialTapTargetSize.shrinkWrap,
                                    ),
                                  ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
