import 'package:flutter/material.dart';
import 'package:flutter_app/provider/auth.dart';
import 'package:flutter_app/drawer.dart';
import 'package:flutter_app/model/product_item.dart';
import 'package:flutter_app/provider/cart_order.dart';
import 'package:flutter_app/provider/order.dart';
import 'package:flutter_app/provider/product.dart';
import 'package:flutter_app/screen/auth_screen.dart';
import 'package:flutter_app/screen/image_picker.dart';
import 'package:flutter_app/screen/product_list_screen.dart';
import 'package:flutter_app/screen/cart_screen.dart';
import 'package:flutter_app/screen/order_screen.dart';
import 'package:flutter_app/screen/product_detail_screen.dart';
import 'package:flutter_app/screen/shop_product_update_screen.dart';
import 'package:provider/provider.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (_) => CartOrderProvider(),
        ),
        ChangeNotifierProvider(
          create: (_) => ProductProvider(),
        ),
        ChangeNotifierProvider(create: (_) => OrderProvider()),
        ChangeNotifierProvider(
          create: (_) => AuthProvider(),
        ),
      ],
      child: Builder(builder: (context) {
        return MaterialApp(
          title: 'Material App',
          home: Provider.of<AuthProvider>(context, listen: true).token != null
              ? const ProductListScreen()
              : const AuthScreen(),
          debugShowCheckedModeBanner: false,
          locale: const Locale('en', 'US'),
          routes: {
            AuthScreen.routeName: (context) => const AuthScreen(),
            ShopProductListScreen.routeName: (context) =>
                const ShopProductListScreen(),
            ProductListScreen.routeName: (context) => const ProductListScreen(),
            CartOrderScreen.routeName: (context) => const CartOrderScreen(),
            OrderScreen.routeName: (context) => const OrderScreen(),
          },
          onGenerateRoute: generateRoute, // generate router
          onUnknownRoute: (settings) => MaterialPageRoute(
            builder: (context) => const Text('Unknown route'),
          ),
          theme: ThemeData(
            textTheme: const TextTheme(
              headline1: TextStyle(
                fontSize: 32.0,
              ),
              headline2: TextStyle(
                fontSize: 30.0,
              ),
              headline3: TextStyle(
                fontSize: 28.0,
              ),
              headline4: TextStyle(
                fontSize: 26.0,
              ),
              headline5: TextStyle(
                fontSize: 24.0,
              ),
              headline6: TextStyle(
                fontSize: 18.0,
              ),
            ),
            buttonTheme: ButtonThemeData(
              buttonColor: Colors.purple,
              textTheme: ButtonTextTheme.primary,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(30.0),
              ),
            ),
            elevatedButtonTheme: ElevatedButtonThemeData(
              style: ElevatedButton.styleFrom(
                primary: Colors.purple,
                padding: const EdgeInsets.symmetric(
                    horizontal: 30.0, vertical: 10.0),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(30.0),
                ),
              ),
            ),
            // fontFamily: 'Lato',
            colorScheme: ColorScheme.fromSwatch(primarySwatch: Colors.blue)
                .copyWith(secondary: Colors.purple),
          ),
        );
      }),
    );
  }

  Route<dynamic>? generateRoute(RouteSettings settings) {
    if (settings.name == ShopProductUpdateScreen.routeName) {
      return MaterialPageRoute(
          builder: (_) {
            //some custom code
            return const ShopProductUpdateScreen();
          },
          settings: settings);
    } else if (settings.name == ProductDetailScreen.routeName) {
      return MaterialPageRoute(
          builder: (_) {
            //some custom code
            return const ProductDetailScreen();
          },
          settings: settings);
    }
    return null;
  }
}

class ShopProductListScreen extends StatefulWidget {
  static const routeName = '/shop-product-list';
  const ShopProductListScreen({Key? key}) : super(key: key);

  @override
  State<ShopProductListScreen> createState() => _ShopProductListScreenState();
}

class _ShopProductListScreenState extends State<ShopProductListScreen> {
  @override
  Widget build(BuildContext context) {
    final productProvider = Provider.of<ProductProvider>(context);
    final items = productProvider.items;
    return Scaffold(
        drawer: const DrawerMenu(),
        appBar: AppBar(
          title: const Text('Your Products'),
          actions: [
            IconButton(
              icon: const Icon(Icons.add),
              onPressed: () async {
                // using static router
                final result = await Navigator.pushNamed(
                    context, ShopProductUpdateScreen.routeName);
                debugPrint('$result');
                setState(() {
                  if (result != null) {
                    // items.sort((a, b) => b.name.compareTo(a.name));
                    // productProvider.addProduct(result as ProductItem);
                  }
                });
              },
            ),
          ],
        ),
        body: ListView.separated(
            itemBuilder: (BuildContext context, int index) {
              final ProductItem item = items[index];
              return ListTile(
                  leading: CircleAvatar(
                    backgroundImage: NetworkImage(item.imageUrl),
                  ),
                  title: Text(item.title),
                  trailing: Row(mainAxisSize: MainAxisSize.min, children: [
                    IconButton(
                        icon: const Icon(
                          Icons.edit,
                          color: Colors.purple,
                        ),
                        onPressed: () async {
                          // using generate router
                          final result = await Navigator.pushNamed(
                            context,
                            ShopProductUpdateScreen.routeName,
                            arguments: item,
                          );
                          productProvider.updateProduct(result as ProductItem);
                        }),
                    IconButton(
                        icon: const Icon(
                          Icons.domain,
                          color: Colors.green,
                        ),
                        onPressed: () {
                          // using dynamic router
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => ShopProductUpdateScreen(
                                item: item,
                              ),
                            ),
                          );
                        }),
                    IconButton(
                        icon: const Icon(
                          Icons.delete,
                          color: Colors.red,
                        ),
                        onPressed: () {
                          productProvider.removeProduct(item);
                        }),
                  ]));
            },
            separatorBuilder: (BuildContext context, int index) =>
                const Divider(),
            itemCount: items.length));
  }
}
